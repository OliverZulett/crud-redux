import {
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCESSFUL,
  ADD_PRODUCT_ERROR,
  FETCH_PRODUCTS_START,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR,
  DELETE_PRODUCT,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR,
  EDIT_PRODUCT,
  EDIT_PRODUCT_SUCCESS,
  EDIT_PRODUCT_ERROR,
} from "../types";

const initialState = {
  products: [],
  error: null,
  loading: false,
  productForDelete: null,
  productForEdit: null,
};

// eslint-disable-next-line
export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_PRODUCT:
    case FETCH_PRODUCTS_START:
      return {
        ...state,
        loading: action.payload,
      };
    case ADD_PRODUCT_SUCCESSFUL:
      return {
        ...state,
        loading: false,
        products: [...state.products, action.payload],
      };
    case FETCH_PRODUCTS_ERROR:
    case ADD_PRODUCT_ERROR:
    case DELETE_PRODUCT_ERROR:
    case EDIT_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        products: action.payload,
      };
    case DELETE_PRODUCT:
      return {
        ...state,
        productForDelete: action.payload,
      };
    case DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        products: state.products.filter(
          (product) => product.id !== state.productForDelete
        ),
        productForDelete: null,
      };
    case EDIT_PRODUCT:
      return {
        ...state,
        productForEdit: action.payload,
      };
    case EDIT_PRODUCT_SUCCESS:
      return {
        ...state,
        productForEdit: null,
        products: state.products.map((product) =>
          product.id === action.payload.id
            ? (product = action.payload)
            : product
        ),
      };
    default:
      return state;
  }
}
