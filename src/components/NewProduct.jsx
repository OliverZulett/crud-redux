import React, { useCallback, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { hideAlert, showAlert } from "../actions/alertActions";
import { createNewProductAction } from "../actions/productActions";

const NewProduct = ({history}) => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);

  const dispatch = useDispatch();

  const loading = useSelector(state => state.products.loading);
  const error = useSelector(state => state.products.error);
  const alert = useSelector(state => state.alert.alert);
  
  const addNewProducts = product => dispatch(createNewProductAction(product));
  const onSubmitNewProduct = useCallback((e) => {
    e.preventDefault();
    if (name.trim === '' || price <= 0) {
      const alert = {
        message: 'All fields are required',
        classes: 'alert alert-danger text-center text-uppercase p3'
      }
      dispatch(showAlert(alert));
      return;
    }
    dispatch(hideAlert());
    addNewProducts({name, price});
    history.push('/');
    // eslint-disable-next-line
  }, [name, price]);
  const onChangeName  = useCallback(
    (e) => {
      setName(e.target.value)
    },
    // eslint-disable-next-line
    [],
  );
  const onChangePrice = useCallback(
    (e) => {
      setPrice(Number(e.target.value))
    },
    // eslint-disable-next-line
    [],
  )
  return (
    <div className="row justify-content-center">
      <div className="col-md-8">
        <div className="card">
          <div className="card-body">
            <h2 className="text-center my-5"> Add new product </h2>
            {alert && <p className={alert.classes}>{alert.message}</p>}
            <form onSubmit={onSubmitNewProduct}>
              <div className="form-group">
                <label>Product Name</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Product Name"
                  name="name"
                  value={name}
                  onChange={onChangeName}
                />
              </div>
              <div className="form-group">
                <label>Product Price</label>
                <input
                  type="number"
                  className="form-control"
                  placeholder="Product Price"
                  name="price"
                  value={price}
                  onChange={onChangePrice}
                />
              </div>
              <button
                type="submit"
                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
              >
                Add
              </button>
            </form>
            {loading && <p>Loading ...</p>}
            {error && <p className="alert alert-danger mt-4 text-center p2"> Error </p>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewProduct;
