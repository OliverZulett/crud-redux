import React, { useEffect, useCallback } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { editProductAction } from "../actions/productActions";

const EditProduct = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [product, setProduct] = useState({
    name: "",
    price: 0,
  });
  const productForUpdate = useSelector(
    (state) => state.products.productForEdit
  );
  // eslint-disable-next-line
  useEffect(() => {
    setProduct(productForUpdate);
  }, [productForUpdate]);
  const onChangeValue = useCallback((e) => {
    setProduct({
      ...product,
      [e.target.name]: e.target.value,
    });
    // eslint-disable-next-line
  }, []);
  const { name, price } = productForUpdate;
  const onSubmitProduct = useCallback((e) => {
    e.preventDefault();
    dispatch(editProductAction(product));
    history.push("/");
    // eslint-disable-next-line
  }, []);
  return (
    <div className="row justify-content-center">
      <div className="col-md-8">
        <div className="card">
          <div className="card-body">
            <h2 className="text-center my-5"> Edit product </h2>
            <form onSubmit={onSubmitProduct}>
              <div className="form-group">
                <label>Product Name</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Product Name"
                  name="name"
                  value={name}
                  onChange={onChangeValue}
                />
              </div>
              <div className="form-group">
                <label>Product Price</label>
                <input
                  type="number"
                  className="form-control"
                  placeholder="Product Price"
                  name="price"
                  value={price}
                  onChange={onChangeValue}
                />
              </div>
              <button
                type="submit"
                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
              >
                Save changes
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditProduct;
