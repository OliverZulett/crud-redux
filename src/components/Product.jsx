import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import {  useHistory } from "react-router-dom";
import { deleteProductFromStorageAction, getProductForEditAction } from "../actions/productActions";
import Swal from "sweetalert2";

const Product = ({ product }) => {
  const { name, price, id } = product;
  const dispatch = useDispatch();
  const history = useHistory();
  const onDeleteProduct = useCallback(
    (id) => {
      Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(deleteProductFromStorageAction(id));
        }
      });
    },
    // eslint-disable-next-line
    [id]
  );
  const onEditProduct = useCallback(
    (id) => {
      console.log(product);
      dispatch(getProductForEditAction(product));
      history.push(`products/edit/${id}`);
    },
    // eslint-disable-next-line
    [id, product]
  );
  return (
    <tr>
      <td>{name}</td>
      <td>
        <span className="font-weight-bold">{price}</span>
      </td>
      <td className="acciones">
        <button
          type="button"
          className="btn btn-primary mr-2"
          onClick={() => onEditProduct(id)}
        >
          Edit
        </button>
        <button
          type="button"
          className="btn btn-danger"
          onClick={() => onDeleteProduct(id)}
        >
          Delete
        </button>
      </td>
    </tr>
  );
};

export default Product;
