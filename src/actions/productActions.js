import Swal from "sweetalert2";
import axiosClient from "../config/axios";
import { ADD_PRODUCT, ADD_PRODUCT_SUCCESSFUL, ADD_PRODUCT_ERROR, FETCH_PRODUCTS_START, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR, DELETE_PRODUCT, DELETE_PRODUCT_SUCCESS, DELETE_PRODUCT_ERROR, EDIT_PRODUCT, EDIT_PRODUCT_ERROR, EDIT_PRODUCT_SUCCESS, EDIT_PRODUCT_START } from "../types";

export function createNewProductAction(product) {
  return async (dispatch) => {
    dispatch(addProduct());
    try {
      await axiosClient.post('/products', product);
      dispatch(addProductSuccessful(product));
      Swal.fire(
        'Correct',
        "Product has been added successful",
        'success'
      )
    } catch (error) {
      console.log(error);
      dispatch(addProductFailded(true));
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'there was an error, try again'
      })
    }
  }
}

const addProduct = () => ({
  type: ADD_PRODUCT,
  payload: true
});

const addProductSuccessful = product => ({
  type: ADD_PRODUCT_SUCCESSFUL,
  payload: product
});

const addProductFailded = errorState => ({
  type: ADD_PRODUCT_ERROR,
  payload: errorState
});

export function fetchProductsFromStorageAction() {
  return async (dispatch) => {
    dispatch(fetchProducts());
    try {
      const resp = await axiosClient.get('/products');
      dispatch(fetchProductsSuccess(resp.data));
    } catch (error) {
      console.log(error);
      dispatch(fetchProductsError());
    }
  }
}

const fetchProducts = () => ({
  type: FETCH_PRODUCTS_START,
  payload: true
});

const fetchProductsSuccess = products => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: products
})

const fetchProductsError = () => ({
  type: FETCH_PRODUCTS_ERROR,
  payload: true
})

export function deleteProductFromStorageAction(id) {
  return async (dispatch) => {
    dispatch(deleteProduct(id));
    try {
      await  axiosClient.delete(`/products/${id}`);
      dispatch(deleteProductSuccessful());
      Swal.fire("Deleted!", "Your product has been deleted.", "success");
    } catch (error) {
      console.log(error);
      dispatch(deleteProducError());
    }
  }
}

const deleteProduct = id => ({
  type: DELETE_PRODUCT,
  payload: id
})

const deleteProductSuccessful = () => ({
  type: DELETE_PRODUCT_SUCCESS
})

const deleteProducError = () => ({
  type: DELETE_PRODUCT_ERROR,
  payload: true
})

export function getProductForEditAction(product) {
  return (dispatch) => {
    dispatch(getProductForEdit(product));
  }
}

const getProductForEdit = product => ({
  type: EDIT_PRODUCT,
  payload: product
});

export function editProductAction(product) {
  return async (dispatch) => {
    dispatch(editProduct(product));
    try {
      await axiosClient.put(`/products/${product.id}`, product);
      dispatch(editProductSuccessful(product));
    } catch (error) {
      console.log(error);
      dispatch(editProductFailded());
    }
  } 
}

const editProduct = () => ({
  type: EDIT_PRODUCT_START,
});

const editProductSuccessful = product => ({
  type: EDIT_PRODUCT_SUCCESS,
  payload: product
});

const editProductFailded = errorState => ({
  type: EDIT_PRODUCT_ERROR,
  payload: errorState
});